package gspot.gcreatives.g_spot;


import android.content.Context;
import android.content.SharedPreferences;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.app.Fragment;


import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements Communicator{

    boolean flag;
    public String name;

    int backcount=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPreferences = getSharedPreferences("LoginData", Context.MODE_PRIVATE);
        flag = sharedPreferences.getBoolean("flag",false);
        name = sharedPreferences.getString("name","User");

        //Toast.makeText(this,name,Toast.LENGTH_SHORT).show();






        if(!flag)
        {
            Show_login();
        }
        else {

           Show_noti_fragment();
            Show_BottomFragment();

        }


    }

    public void Show_noti_fragment()
    {
        Notification_activity notify_frag = new Notification_activity();
        android.app.FragmentManager notify_manager = getFragmentManager();
        FragmentTransaction notify_transaction = notify_manager.beginTransaction();
        notify_transaction.add(R.id.activity_main, notify_frag, "NOTIFICATION LIST FRAGMENT");
        notify_transaction.commit();


    }

    public void Show_BottomFragment()
    {
        BottombarFragment bottom_frag = new BottombarFragment();
        android.app.FragmentManager notify_manager = getFragmentManager();
        FragmentTransaction bottom_transaction = notify_manager.beginTransaction();
        bottom_transaction.add(R.id.activity_main, bottom_frag, "BOTTOM BAR FRAGMENT");
        bottom_transaction.commit();




    }





    public void Show_login() {
       android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        Login_page frag1 = new Login_page();
        frag1.show(manager,"LOGIN PAGE");
        //FragmentTransaction login_transaction = manager.beginTransaction();
        //login_transaction.add(R.id.activity_main, frag1, "LOGIN PAGE");
        //login_transaction.commit();


    }

    @Override
    public void notify_inflator() {
        Show_noti_fragment();
        Show_BottomFragment();
    }

    @Override
    public void ShowAccountfrag() {
        Notification_activity notf = (Notification_activity)getFragmentManager().findFragmentByTag("NOTIFICATION LIST FRAGMENT");
        notf.hideme();
        AccountFragment account_frag = new AccountFragment();
        FragmentManager acnt_manager = getFragmentManager();
        FragmentTransaction account_transaction = acnt_manager.beginTransaction();
        account_transaction.add(R.id.activity_main, account_frag,"ACFRAG");
        account_transaction.commit();



    }

    @Override
    public void logout() {
        notifykiller();
        accountkiller();
        bottomkiller();
        Show_login();

    }

    @Override
    public void namegiver() {

        AccountFragment acntfrg = (AccountFragment)getFragmentManager().findFragmentByTag("ACFRAG");
        acntfrg.setUsername(name);

    }

    public void bottomkiller(){

        BottombarFragment botm = (BottombarFragment)getFragmentManager().findFragmentByTag("BOTTOM BAR FRAGMENT");
        if(botm!=null)
            botm.killme();
    }

    public void notifykiller()
    {
        Notification_activity notf = (Notification_activity)getFragmentManager().findFragmentByTag("NOTIFICATION LIST FRAGMENT");
        if(notf!=null)
            notf.killme();
    }

    @Override
    public void accountkiller() {
        Notification_activity notf = (Notification_activity)getFragmentManager().findFragmentByTag("NOTIFICATION LIST FRAGMENT");
        notf.showme();
        AccountFragment acntfrg = (AccountFragment)getFragmentManager().findFragmentByTag("ACFRAG");
        if(acntfrg!=null)
        acntfrg.killme();
    }

    @Override
    public void flagconnector(Boolean kodi) {

        SharedPreferences sharedPreferences = getSharedPreferences("LoginData",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("flag",kodi);
        editor.commit();
    }

    @Override
    public void nameconnector(String s) {
        SharedPreferences sharedPreferences = getSharedPreferences("LoginData",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("name",s);
        editor.commit();
        name=s;


    }

    @Override
    public void onBackPressed() {
        if(backcount==0) {
            Toast.makeText(this, "Press back again to exit!", Toast.LENGTH_SHORT).show();
            backcount++;
        }
        else
            killer();
    }


    @Override
    public void killer()
    {
        finish();
    }



}
