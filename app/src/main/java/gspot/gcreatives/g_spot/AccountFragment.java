package gspot.gcreatives.g_spot;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * Created by Key Smash on 23-Jun-17.
 */

public class AccountFragment extends Fragment implements View.OnClickListener {

    TextView username;
    ImageButton logout;

    Communicator communicator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        
        View view = inflater.inflate(R.layout.account_layout,container,false);

        logout = (ImageButton) view.findViewById(R.id.Logoutbutton);
        username = (TextView) view.findViewById(R.id.Username);


        logout.setOnClickListener(this);

        communicator = (Communicator) getActivity();




        communicator.namegiver();



        return view;
    }

    public void killme(){
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }

    public void setUsername(String str)
    {
        username.setText("Hey "+str);
    }

    @Override
    public void onClick(View view) {

        communicator.flagconnector(false);
        communicator.logout();

    }
}
