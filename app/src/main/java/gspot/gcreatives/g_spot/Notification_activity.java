package gspot.gcreatives.g_spot;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.app.ProgressDialog;
import android.widget.Toast;

import java.io.File;


public class Notification_activity extends Fragment {

    private TextView result;
   SwipeRefreshLayout srl;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.notify_layout,container,false);
        result = (TextView) view.findViewById(R.id.result);
        srl= (SwipeRefreshLayout) view.findViewById(R.id.SwipeRefresh);
        if(result.getVisibility()==View.INVISIBLE)
            result.setVisibility(View.VISIBLE);
        String fileUrl = "/Android/data/com.roshanshibu.gammafeed/data.txt";
        String file = android.os.Environment.getExternalStorageDirectory().getPath() + fileUrl;
        final File f = new File(file);

        final SwipeRefreshLayout swipeView = (SwipeRefreshLayout) view.findViewById(R.id.SwipeRefresh);
        swipeView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                swipeView.setRefreshing(true);
                Thread t =new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            final Document NotfData = Jsoup.connect("https://csegamma.000webhostapp.com/notifications.html").get();
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String notfs = NotfData.toString();
                                    notfs=notfs.substring(32,notfs.length()-15);
                                    notfs=notfs+"<br/><br/><br/>";

                                    if (FileHelper.saveToFile(notfs)){
                                        Toast.makeText(getActivity(),"Notifications updated yo!",Toast.LENGTH_SHORT).show();
                                    }else{
                                        Toast.makeText(getActivity(),"Error in saving file!",Toast.LENGTH_SHORT).show();
                                    }
                                    swipeView.setRefreshing(false);


                                    result.setText(Html.fromHtml(notfs), TextView.BufferType.SPANNABLE);
                                }
                            });
                        }
                        catch (Throwable e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (f.exists())
                                        result.setText(Html.fromHtml(FileHelper.ReadFile(getActivity())), TextView.BufferType.SPANNABLE);
                                    else
                                        result.setText("Failed to Load Notifications.\nConnect to the internet and try again");
                                }
                            });
                        }

                    }
                });
                t.start();



            }
        });




        if(f.exists())
            result.setText(Html.fromHtml(FileHelper.ReadFile(getActivity())), TextView.BufferType.SPANNABLE);
        else
            swipeView.performClick();

        return view;
    }

    public void killme(){
        getActivity().getFragmentManager().beginTransaction().remove(this).commit();
    }
    public void hideme(){
        srl.setVisibility(View.GONE);
    }
    public void showme(){
        if(srl.getVisibility()==View.GONE)
        srl.setVisibility(View.VISIBLE);
    }

}
